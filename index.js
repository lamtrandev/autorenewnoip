const puppeteer = require('puppeteer');
var CREDS = require('./config.json');
var fs = require('fs');

const LOGIN_PG =  'https://www.noip.com/login';
const PASSWORD_SELECTOR =  '#clogs > input:nth-child(2)';
const USERNAME_SELECTOR =  '#clogs > input:nth-child(1)';
const BUTTON_SELECTOR =  '#clogs > button';
const CONFIRM_BUTTON_SELECTOR =  '#host-panel > table > tbody > tr > td:nth-child(5) > button';

async function run() {
  const browser = await puppeteer.launch({
  headless: true
});
  const page = await browser.newPage();

  // Load login page
  await page.goto(LOGIN_PG);
  page.once('load', () => console.log('Page loaded!'));

  // Fill in username and password
  await page.click(USERNAME_SELECTOR);
  await page.keyboard.type(CREDS.username);

  await page.click(PASSWORD_SELECTOR);
  await page.keyboard.type(CREDS.password);

  await page.click(BUTTON_SELECTOR);

  await page.waitForNavigation();
  let d = new Date();
  let dir = d.getMonth()+""+d.getDate()
  
  if (!fs.existsSync(dir)){
    fs.mkdirSync(dir);
  }

  await page.screenshot({path: dir + '/p1.png'});
  // Open ddns page to renew and click confirm button
  page2 = await browser.newPage();
  await page2.goto('https://my.noip.com/#!/dynamic-dns');
  await page2.click(CONFIRM_BUTTON_SELECTOR);
  await page.screenshot({path: dir + '/p2.png'});
  browser.close();
}

run();
